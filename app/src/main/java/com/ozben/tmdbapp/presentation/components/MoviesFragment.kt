package com.ozben.tmdbapp.presentation.components

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
import com.ozben.tmdbapp.databinding.FragmentMoviesBinding
import com.ozben.tmdbapp.presentation.components.recycleradapters.MoviesAdapter

class MoviesFragment : Fragment() {

    private lateinit var binding: FragmentMoviesBinding
    private lateinit var viewModel: MoviesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel= ViewModelProvider(this, (activity as MainActivity).factory)[MoviesViewModel::class.java]
        binding = FragmentMoviesBinding.inflate(inflater, container, false).apply {
            viewmodel = viewModel
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = this.viewLifecycleOwner
        binding.moviesRecycler.apply {
            val dividerItemDecoration = DividerItemDecoration(
                requireContext(),
                VERTICAL
            )
            addItemDecoration(dividerItemDecoration)
            adapter = MoviesAdapter()
        }

        viewModel.syncData()
    }
}