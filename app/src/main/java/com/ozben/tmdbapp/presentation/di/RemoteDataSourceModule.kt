package com.ozben.tmdbapp.presentation.di

import com.ozben.tmdbapp.data.api.RemoteService
import com.ozben.tmdbapp.data.repository.genres.datasource.GenresRemoteDataSource
import com.ozben.tmdbapp.data.repository.genres.datasourceimpl.GenresRemoteDataSourceImpl
import com.ozben.tmdbapp.data.repository.movies.datasource.MoviesRemoteDataSource
import com.ozben.tmdbapp.data.repository.movies.datasourceimpl.MoviesRemoteDataSourceImp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RemoteDataSourceModule {

    @Singleton
    @Provides
    fun provideMoviesRemoteDataSource(remoteService: RemoteService): MoviesRemoteDataSource {
        return MoviesRemoteDataSourceImp(
            remoteService
        )
    }

    @Singleton
    @Provides
    fun provideGenresRemoteDataSource(remoteService: RemoteService): GenresRemoteDataSource {
        return GenresRemoteDataSourceImpl(
            remoteService
        )
    }
}