package com.ozben.tmdbapp.presentation

import android.app.Application
import com.ozben.tmdbapp.presentation.components.MainActivity
import com.ozben.tmdbapp.presentation.di.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    NetworkModule::class,
    LocalDataSourceModule::class,
    RemoteDataSourceModule::class,
    UseCaseModule::class,
    RepositoryModule::class,
    ViewModelModule::class
])
interface ApplicationComponent {
    fun inject(activity: MainActivity)
}

class TmdbApp: Application() {
    val appComponent: ApplicationComponent = DaggerApplicationComponent.create()
}