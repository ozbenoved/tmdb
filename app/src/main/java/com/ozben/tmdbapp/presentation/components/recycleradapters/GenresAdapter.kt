package com.ozben.tmdbapp.presentation.components.recycleradapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ozben.tmdbapp.R
import com.ozben.tmdbapp.databinding.GenreItemBinding

class GenresAdapter(val genres: List<String>):RecyclerView.Adapter<GenreViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding : GenreItemBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.genre_item,
            parent,
            false
        )
        return GenreViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return genres.size
    }

    override fun onBindViewHolder(holder: GenreViewHolder, position: Int) {
       holder.bind(genres[position])
    }
}



class GenreViewHolder(private val binding: GenreItemBinding):
RecyclerView.ViewHolder(binding.root){

   fun bind(genre: String){
        binding.genreName.text = genre
   }
}