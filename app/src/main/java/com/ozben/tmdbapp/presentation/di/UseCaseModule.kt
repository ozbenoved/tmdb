package com.ozben.tmdbapp.presentation.di

import com.ozben.tmdbapp.domain.repository.GenresRepository
import com.ozben.tmdbapp.domain.repository.MoviesRepository
import com.ozben.tmdbapp.domain.usecase.GetGenresUseCase
import com.ozben.tmdbapp.domain.usecase.GetMoviesUseCase
import dagger.Module
import dagger.Provides

@Module
class UseCaseModule {
    @Provides
    fun provideMoviesUseCase(movieRepository: MoviesRepository): GetMoviesUseCase {
        return GetMoviesUseCase(movieRepository)
    }

    @Provides
    fun provideGeneresUseCase(genreRepository: GenresRepository): GetGenresUseCase {
        return GetGenresUseCase(genreRepository)
    }

}