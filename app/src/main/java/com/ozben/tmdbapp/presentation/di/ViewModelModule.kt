package com.ozben.tmdbapp.presentation.di

import com.ozben.tmdbapp.domain.usecase.GetGenresUseCase
import com.ozben.tmdbapp.domain.usecase.GetMoviesUseCase
import com.ozben.tmdbapp.presentation.components.MoviesViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ViewModelModule {
    @Singleton
    @Provides
    fun provideMoviesViewModelFactory(
        getMoviesUseCase: GetMoviesUseCase,
        getGenresUseCase: GetGenresUseCase
    ) : MoviesViewModelFactory {
        return MoviesViewModelFactory(getMoviesUseCase, getGenresUseCase)
    }
}