package com.ozben.tmdbapp.presentation.di

import com.ozben.tmdbapp.data.repository.genres.GenresRepositoryImpl
import com.ozben.tmdbapp.data.repository.genres.datasource.GenresLocalDataSource
import com.ozben.tmdbapp.data.repository.genres.datasource.GenresRemoteDataSource
import com.ozben.tmdbapp.data.repository.movies.MoviesRepositoryImpl
import com.ozben.tmdbapp.data.repository.movies.datasource.MoviesLocalDataSource
import com.ozben.tmdbapp.data.repository.movies.datasource.MoviesRemoteDataSource
import com.ozben.tmdbapp.domain.repository.GenresRepository
import com.ozben.tmdbapp.domain.repository.MoviesRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Provides
    @Singleton
    fun provideMoviesRepository(
        moviesRemoteDatasource: MoviesRemoteDataSource,
        moviesLocalDataSource: MoviesLocalDataSource,
    ): MoviesRepository {

        return MoviesRepositoryImpl(
            moviesRemoteDatasource,
            moviesLocalDataSource,
        )
    }

    @Provides
    @Singleton
    fun provideGenresRepository(
        genresRemoteDataSource: GenresRemoteDataSource,
        genresLocalDataSource: GenresLocalDataSource,
    ): GenresRepository {

        return GenresRepositoryImpl(
            genresRemoteDataSource,
            genresLocalDataSource,
        )
    }
}