package com.ozben.tmdbapp.presentation.components.recycleradapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ozben.tmdbapp.R
import com.ozben.tmdbapp.data.model.Genre
import com.ozben.tmdbapp.data.model.Movie
import com.ozben.tmdbapp.databinding.MovieItemBinding

class MoviesAdapter : RecyclerView.Adapter<MovieViewHolder>() {

    private val _movies: MutableList<Movie> = mutableListOf()
    private var _genres = mutableMapOf<Int, String>()

    var movies: List<Movie>
    get() = _movies
    set(value) {
        _movies.clear()
        _movies.addAll(value)
        this.notifyItemRangeChanged(0, _movies.size)
    }

    fun setGenres(genres: List<Genre>) {
        genres.map {
            _genres[it.id] = it.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding : MovieItemBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.movie_item,
            parent,
            false
        )
        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val currentMovie = _movies[position]
        val currentGenres = mutableListOf<String>()
        currentMovie.genre_ids.forEach {
            _genres[it]?.let { genreName ->
                currentGenres.add(genreName)
            }
        }
       holder.bind(_movies[position], currentGenres.toList())
    }

    override fun getItemCount(): Int {
        return _movies.size
    }

}

class MovieViewHolder(private val binding: MovieItemBinding):
RecyclerView.ViewHolder(binding.root){

   fun bind(movie: Movie, genres: List<String>){
       binding.genres.adapter = GenresAdapter(genres)
       binding.date.text = movie.release_date
       binding.title.text = movie.title
   }
}