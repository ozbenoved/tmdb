package com.ozben.tmdbapp.presentation.di

import com.ozben.tmdbapp.data.repository.genres.datasource.GenresLocalDataSource
import com.ozben.tmdbapp.data.repository.genres.datasourceimpl.GenresLocalDataSourceImpl
import com.ozben.tmdbapp.data.repository.movies.datasource.MoviesLocalDataSource
import com.ozben.tmdbapp.data.repository.movies.datasourceimpl.MoviesLocalDataSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocalDataSourceModule {

    @Singleton
    @Provides
    fun provideMoviesLocalDataSource(): MoviesLocalDataSource{
        return MoviesLocalDataSourceImpl()
    }

    @Singleton
    @Provides
    fun provideGenresLocalDataSource(): GenresLocalDataSource{
        return GenresLocalDataSourceImpl()
    }

}