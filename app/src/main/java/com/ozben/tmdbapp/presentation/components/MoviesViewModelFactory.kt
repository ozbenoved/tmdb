package com.ozben.tmdbapp.presentation.components

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ozben.tmdbapp.domain.usecase.GetGenresUseCase
import com.ozben.tmdbapp.domain.usecase.GetMoviesUseCase

class MoviesViewModelFactory(
    private val getMoviesUseCase: GetMoviesUseCase,
    private val getGenresUseCase: GetGenresUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MoviesViewModel(
            getMoviesUseCase,
            getGenresUseCase
        ) as T
    }

}