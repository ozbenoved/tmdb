package com.ozben.tmdbapp.presentation.components

import androidx.lifecycle.*
import com.ozben.tmdbapp.data.model.Genre
import com.ozben.tmdbapp.data.model.Movie
import com.ozben.tmdbapp.domain.usecase.GetGenresUseCase
import com.ozben.tmdbapp.domain.usecase.GetMoviesUseCase
import kotlinx.coroutines.launch
import javax.inject.Inject

class MoviesViewModel @Inject constructor(
    private val getMoviesUseCase: GetMoviesUseCase,
    private val getGenresUseCase: GetGenresUseCase
) : ViewModel() {

    private var _genres= MutableLiveData<List<Genre>>()
    val genres: LiveData<List<Genre>> = _genres

    private var _movies= MutableLiveData<List<Movie>>()
    val movies: LiveData<List<Movie>> = _movies

    fun syncData() {
        viewModelScope.launch {
            _genres.postValue(getGenresUseCase.execute())
            _movies.postValue(getMoviesUseCase.execute())
        }
    }
}