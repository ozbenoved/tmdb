package com.ozben.tmdbapp.presentation.components

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.ozben.tmdbapp.R
import com.ozben.tmdbapp.databinding.ActivityMainBinding
import com.ozben.tmdbapp.presentation.TmdbApp
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var factory: MoviesViewModelFactory
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as TmdbApp).appComponent.inject(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        supportFragmentManager
            .beginTransaction()
            .replace(binding.container.id, MoviesFragment())
            .commit()
    }
}