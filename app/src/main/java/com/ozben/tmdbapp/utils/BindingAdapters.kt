package com.ozben.tmdbapp.utils

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ozben.tmdbapp.data.model.Genre
import com.ozben.tmdbapp.data.model.Movie
import com.ozben.tmdbapp.presentation.components.recycleradapters.MoviesAdapter

/**
 * [BindingAdapter]s for the [Movie]s list.
 */
@BindingAdapter("app:movies")
fun setMovies(listView: RecyclerView, items: List<Movie>?) {
    items?.let {
        (listView.adapter as MoviesAdapter).movies = items
    }
}

/**
 * [BindingAdapter]s for the [Genre]s list.
 */
@BindingAdapter("app:allGenres")
fun setAllGenres(listView: RecyclerView, items: List<Genre>?) {
    items?.let {
        (listView.adapter as MoviesAdapter).setGenres(items)
    }
}