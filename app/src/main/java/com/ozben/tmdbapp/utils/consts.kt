package com.ozben.tmdbapp.utils

object Consts {

    const val BaseUrl = "https://api.themoviedb.org/3/"
    const val MoviesPath = "discover/movie"
    const val GenresPath = "genre/movie/list"
    const val API_KEY = "b25ada0aaa9911135fa454b2ad8494be"
    const val NumOfMovies = 100
    const val Year = 2014
}