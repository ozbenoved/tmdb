package com.ozben.tmdbapp.data.repository.genres.datasource

import com.ozben.tmdbapp.data.model.Genre

interface GenresLocalDataSource {
    suspend fun getGenres(): List<Genre>
    suspend fun saveGenres(genres: List<Genre>)
}