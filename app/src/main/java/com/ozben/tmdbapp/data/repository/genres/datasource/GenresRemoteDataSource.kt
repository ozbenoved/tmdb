package com.ozben.tmdbapp.data.repository.genres.datasource

import com.ozben.tmdbapp.data.model.GenreList

interface GenresRemoteDataSource {
    suspend fun getGenres(): GenreList
}