package com.ozben.tmdbapp.data.model

data class Genre(
    val id: Int,
    val name: String
)