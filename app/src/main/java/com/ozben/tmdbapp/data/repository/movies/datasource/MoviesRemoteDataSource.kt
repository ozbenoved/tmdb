package com.ozben.tmdbapp.data.repository.movies.datasource

import com.ozben.tmdbapp.data.model.MoviesResult

interface MoviesRemoteDataSource {
    suspend fun getMovies(year: Int, page: Int): MoviesResult
}