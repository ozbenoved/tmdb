package com.ozben.tmdbapp.data.repository.genres.datasourceimpl

import com.ozben.tmdbapp.data.api.RemoteService
import com.ozben.tmdbapp.data.model.GenreList
import com.ozben.tmdbapp.data.repository.genres.datasource.GenresRemoteDataSource
import com.ozben.tmdbapp.utils.Consts.API_KEY
import javax.inject.Inject
import javax.inject.Singleton

class GenresRemoteDataSourceImpl(private val remoteService: RemoteService): GenresRemoteDataSource {
    override suspend fun getGenres(): GenreList =
        remoteService.getGenres(API_KEY)
}