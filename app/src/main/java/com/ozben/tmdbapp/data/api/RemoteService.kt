package com.ozben.tmdbapp.data.api

import com.ozben.tmdbapp.data.model.GenreList
import com.ozben.tmdbapp.data.model.MoviesResult
import com.ozben.tmdbapp.utils.Consts.GenresPath
import com.ozben.tmdbapp.utils.Consts.MoviesPath
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteService {

    @GET(MoviesPath)
    suspend fun getMovies(
        @Query(
            "api_key"
        ) apiKey: String,
        @Query(
            "page"
        ) page: Int,
        @Query(
            "primary_release_year"
        ) year: Int
    ): MoviesResult

    @GET(GenresPath)
    suspend fun getGenres(
        @Query(
            "api_key"
        ) apiKey: String,
    ): GenreList
}
