package com.ozben.tmdbapp.data.repository.movies

import com.ozben.tmdbapp.data.model.Movie
import com.ozben.tmdbapp.data.repository.movies.datasource.MoviesLocalDataSource
import com.ozben.tmdbapp.data.repository.movies.datasource.MoviesRemoteDataSource
import com.ozben.tmdbapp.domain.repository.MoviesRepository
import com.ozben.tmdbapp.utils.Consts.NumOfMovies
import com.ozben.tmdbapp.utils.Consts.Year
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoviesRepositoryImpl(
    private val remoteDataSourceDataSource: MoviesRemoteDataSource,
    private val localDataSourceDataSource: MoviesLocalDataSource
) : MoviesRepository {

    override suspend fun syncMovies(): List<Movie> {

        // 1888 is the year of the first movie ever made (by https://en.wikipedia.org/wiki/History_of_film)
        if (NumOfMovies < 1 || Year < 1888) return emptyList()

        var movies = localDataSourceDataSource.getMovies()
        if (movies.isEmpty()) {
            withContext(Dispatchers.IO) {
                movies = fetchMoviesFromRemote(Year, NumOfMovies)
                localDataSourceDataSource.saveMovies(movies)
                movies = localDataSourceDataSource.getMovies()
            }
        }
        return movies
    }

    private suspend fun fetchMoviesFromRemote(year: Int, numberOfMovies: Int): List<Movie> {

        val searchResult = remoteDataSourceDataSource.getMovies(year, 1)

        searchResult.results.toMutableList().let { movies ->

            when {
                movies.isEmpty() -> return movies

                movies.size > numberOfMovies -> return movies.subList(0, numberOfMovies)

                else -> {
                    var page = 2;
                    do {
                        coroutineScope {
                            launch {
                                val fetched =
                                    remoteDataSourceDataSource.getMovies(year, page).results
                                movies.addAll(fetched)
                                page++
                            }.join()
                        }

                    } while (page <= searchResult.total_pages && movies.size < numberOfMovies)

                    if (movies.size > numberOfMovies) {
                        return movies.subList(0, numberOfMovies)
                    }

                    return movies
                }
            }
        }
    }
}