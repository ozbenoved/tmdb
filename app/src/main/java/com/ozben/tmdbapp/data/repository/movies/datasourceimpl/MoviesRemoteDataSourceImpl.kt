package com.ozben.tmdbapp.data.repository.movies.datasourceimpl

import com.ozben.tmdbapp.data.api.RemoteService
import com.ozben.tmdbapp.data.model.MoviesResult
import com.ozben.tmdbapp.data.repository.movies.datasource.MoviesRemoteDataSource
import com.ozben.tmdbapp.utils.Consts.API_KEY
import javax.inject.Inject
import javax.inject.Singleton

class MoviesRemoteDataSourceImp(private val remoteService: RemoteService): MoviesRemoteDataSource {

    override suspend fun getMovies(year: Int, page: Int): MoviesResult =
        remoteService.getMovies(API_KEY, page, year)
}