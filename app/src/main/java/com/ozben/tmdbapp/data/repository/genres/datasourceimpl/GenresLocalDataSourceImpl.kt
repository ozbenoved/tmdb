package com.ozben.tmdbapp.data.repository.genres.datasourceimpl

import com.ozben.tmdbapp.data.model.Genre
import com.ozben.tmdbapp.data.repository.genres.datasource.GenresLocalDataSource
import javax.inject.Inject
import javax.inject.Singleton

class GenresLocalDataSourceImpl: GenresLocalDataSource {

    private var genres = listOf<Genre>()

    override suspend fun getGenres(): List<Genre> =
        // returns a copy of the genres list (to avoid direct modifications)
       ArrayList(genres)

    override suspend fun saveGenres(genres: List<Genre>) {
        this.genres = genres
    }

}