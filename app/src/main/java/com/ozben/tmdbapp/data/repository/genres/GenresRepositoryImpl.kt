package com.ozben.tmdbapp.data.repository.genres

import com.ozben.tmdbapp.data.model.Genre
import com.ozben.tmdbapp.data.repository.genres.datasource.GenresLocalDataSource
import com.ozben.tmdbapp.data.repository.genres.datasource.GenresRemoteDataSource
import com.ozben.tmdbapp.domain.repository.GenresRepository
import javax.inject.Inject
import javax.inject.Singleton

class GenresRepositoryImpl(private val remoteDataSourceDataSource: GenresRemoteDataSource, private val genresLocalDataSource: GenresLocalDataSource) :
    GenresRepository {

    override suspend fun getGenres(): List<Genre> {
        var genresMap = genresLocalDataSource.getGenres()
        if (genresMap.isEmpty()) {
            val genresList = remoteDataSourceDataSource.getGenres()
            genresLocalDataSource.saveGenres(genresList.genres)
            genresMap = genresLocalDataSource.getGenres()
        }
        return genresMap
    }
}