package com.ozben.tmdbapp.data.repository.movies.datasourceimpl

import com.ozben.tmdbapp.data.model.Movie
import com.ozben.tmdbapp.data.repository.movies.datasource.MoviesLocalDataSource
import javax.inject.Inject
import javax.inject.Singleton

class MoviesLocalDataSourceImpl : MoviesLocalDataSource {

    private var movieList = listOf<Movie>()

    override suspend fun getMovies(): List<Movie> =
        // returns a copy of the movie list (to avoid direct modifications)
        ArrayList(movieList)

    override suspend fun saveMovies(movies: List<Movie>) {
        movieList = movies
    }
}