package com.ozben.tmdbapp.data.repository.movies.datasource

import com.ozben.tmdbapp.data.model.Movie

interface MoviesLocalDataSource {
    suspend fun getMovies(): List<Movie>
    suspend fun saveMovies(movies: List<Movie>)
}