package com.ozben.tmdbapp.data.model

data class GenreList(
    val genres: List<Genre>
)