package com.ozben.tmdbapp.domain.usecase

import com.ozben.tmdbapp.data.model.Genre
import com.ozben.tmdbapp.domain.repository.GenresRepository
import javax.inject.Inject

class GetGenresUseCase @Inject constructor(private val genresRepository: GenresRepository) {
    suspend fun execute(): List<Genre> = genresRepository.getGenres()
}