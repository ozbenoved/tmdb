package com.ozben.tmdbapp.domain.repository

import com.ozben.tmdbapp.data.model.Movie

interface MoviesRepository {
    suspend fun syncMovies(): List<Movie>
}