package com.ozben.tmdbapp.domain.usecase

import com.ozben.tmdbapp.data.model.Movie
import com.ozben.tmdbapp.domain.repository.MoviesRepository
import javax.inject.Inject

class GetMoviesUseCase @Inject constructor(private val getMoviesRepository: MoviesRepository) {
    suspend fun execute(): List<Movie> = getMoviesRepository.syncMovies()
}