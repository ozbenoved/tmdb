package com.ozben.tmdbapp.domain.repository

import com.ozben.tmdbapp.data.model.Genre

interface GenresRepository {
    suspend fun getGenres(): List<Genre>
}